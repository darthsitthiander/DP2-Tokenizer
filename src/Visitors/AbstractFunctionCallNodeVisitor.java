package Visitors;

import Nodes.*;

public class AbstractFunctionCallNodeVisitor extends NodeVisitor
{
	private AbstractFunctionCallNode abstractFunctionCallNode;

	public AbstractFunctionCallNode getAbstractFunctionCallNode ()								{ return this.abstractFunctionCallNode;							}
	public void setAbstractFunctionCallNode (AbstractFunctionCallNode abstractFunctionCallNode)	{ this.abstractFunctionCallNode = abstractFunctionCallNode;		}

	@Override
	public void visit (AbstractFunctionCallNode abstractFunctionCallNode)						{ this.setAbstractFunctionCallNode(abstractFunctionCallNode);	}

	@Override
	public void visit (ConditionalJumpNode node)												{ this.setAbstractFunctionCallNode(null);						}

	@Override
	public void visit (DoNothingNode node)														{ this.setAbstractFunctionCallNode(null);						}

	@Override
	public void visit (JumpNode node)															{ this.setAbstractFunctionCallNode(null);						}
}
