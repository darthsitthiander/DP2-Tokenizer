package Visitors;

import Nodes.*;

public abstract class NodeVisitor
{
	public abstract void visit(ConditionalJumpNode node);
	public abstract void visit(DoNothingNode node);
	public abstract void visit(AbstractFunctionCallNode node);
	public abstract void visit(JumpNode node);
}
