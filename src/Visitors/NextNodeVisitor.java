package Visitors;

import Nodes.*;
import VirtualMachine.VirtualMachine;
import enums.BooleanType;

public class NextNodeVisitor extends NodeVisitor
{
	private Node nextNode;

	public Node getNextNode ()																	{ return this.nextNode;						}
	public void setNextNode (Node nextNode)														{ this.nextNode = nextNode;					}

	@Override
	public void visit (ConditionalJumpNode node)
	{
		if (VirtualMachine.getInstance().getReturnValue().equals(BooleanType.TRUE.toString()))	{ this.setNextNode(node.getNextOnTrue());	}
		else																					{ this.setNextNode(node.getNextOnFalse());	}
	}

	@Override
	public void visit (DoNothingNode node)														{ this.setNextNode(node.getNext());			}

	@Override
	public void visit (AbstractFunctionCallNode node)											{ this.setNextNode(node.getNext());			}

	@Override
	public void visit (JumpNode node)															{ this.setNextNode(node.getJumpToNode()); }
}
