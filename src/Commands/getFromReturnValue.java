package Commands;

import VirtualMachine.VirtualMachine;


public class getFromReturnValue extends Command
{
	@Override
	public void execute(VirtualMachine vm, String[] parameters)	{ vm.setVariable(parameters[1], vm.getReturnValue()); }
}
