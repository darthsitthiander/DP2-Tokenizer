package Commands;

import VirtualMachine.VirtualMachine;

public class SetIdentifierToReturnValue extends Command
{
	@Override
	public void execute(VirtualMachine vm, String[] parameters)	{ vm.setReturnValue(vm.getVariable(parameters[1])); }
}
