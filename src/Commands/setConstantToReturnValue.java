package Commands;

import VirtualMachine.VirtualMachine;

public class setConstantToReturnValue extends Command
{
	@Override
	public void execute(VirtualMachine vm, String[] parameters)	{ vm.setReturnValue(parameters[1]); }
}
