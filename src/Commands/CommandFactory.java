package Commands;

import java.util.Iterator;
import java.util.ServiceConfigurationError;
import java.util.ServiceLoader;

public class CommandFactory
{
	private static CommandFactory instance;
	private ServiceLoader<Command> loader;

	private CommandFactory()							{ loader = ServiceLoader.load(Command.class); }

	public static synchronized CommandFactory getInstance()
	{
		if (instance == null)							{ instance = new CommandFactory(); }

		return instance;
	}
	public Command getCommand (final String command)
	{
		Command commandStrategy							= null;
		Command strategy;

		try
		{
			Iterator<Command> commandStrategies			= loader.iterator();

			while (commandStrategy == null && commandStrategies.hasNext())
			{
				strategy								= commandStrategies.next();

				if (strategy.getClass().getSimpleName().equalsIgnoreCase(command))
				{
					commandStrategy						= strategy.clone();
				}
			}
		}
		catch (ServiceConfigurationError serviceError)
		{
			System.out.println("ServiceConfigurationError: " + serviceError.getMessage() + ". Exiting program.");
			System.exit(1);
		}

		return commandStrategy;
	}
}
