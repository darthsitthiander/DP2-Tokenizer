package Commands;

import Tokenizer.TypeHelper;
import VirtualMachine.VirtualMachine;

public class multiply extends Command
{

	@Override
	public void execute (VirtualMachine vm, String[] parameters)
	{
		String var1														= vm.getVariable(parameters[1]);
		String var2														= vm.getVariable(parameters[2]);

		if (TypeHelper.isNumeric(var1) && TypeHelper.isNumeric(var2))	{ vm.setReturnValue(Integer.toString(Integer.parseInt(var1) * Integer.parseInt(var2))); }
		else															{ vm.resetReturnValue(); }
	}
}
