package Commands;

import Tokenizer.TypeHelper;
import VirtualMachine.VirtualMachine;

public class sqrt extends Command
{

	@Override
	public void execute (VirtualMachine vm, String[] parameters)
	{
		String var1														= vm.getVariable(parameters[1]);

		if (TypeHelper.isNumeric(var1))									{ vm.setReturnValue(Double.toString(Math.sqrt(Double.parseDouble(var1)))); }
		else															{ vm.setReturnValue(null); }
	}
}
