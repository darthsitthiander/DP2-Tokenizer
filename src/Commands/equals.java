package Commands;

import Tokenizer.TypeHelper;
import VirtualMachine.VirtualMachine;
import enums.BooleanType;

public class equals extends Command
{

	@Override
	public void execute (VirtualMachine vm, String[] parameters)
	{
		String var1														= vm.getVariable(parameters[1]);
		String var2														= vm.getVariable(parameters[2]);

		if (var1.equals(var2))											{ vm.setReturnValue(BooleanType.TRUE.toString()); }
		else															{ vm.setReturnValue(BooleanType.FALSE.toString()); }
	}
}
