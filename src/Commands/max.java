package Commands;

import Tokenizer.TypeHelper;
import VirtualMachine.VirtualMachine;

public class max extends Command
{

	@Override
	public void execute (VirtualMachine vm, String[] parameters)
	{
		int max									= Integer.MIN_VALUE;
		int paramInt;
		Double paramDouble;

		for (int i = 1; i < parameters.length; i++)
		{
			if (vm.getVariable(parameters[i]) != null && TypeHelper.isNumeric(vm.getVariable(parameters[i])))
			{
				if (TypeHelper.isDouble(vm.getVariable(parameters[i])))
				{
					paramDouble					= Double.parseDouble(vm.getVariable(parameters[i]));
					paramInt					= paramDouble.intValue();
				}
				else
				{
					paramInt = Integer.parseInt(vm.getVariable(parameters[i]));
				}

				if (paramInt > max)				{ max = paramInt; }
			}
		}

		if (max > Integer.MIN_VALUE)			{ vm.setReturnValue(Integer.toString(max));	}
		else									{ vm.resetReturnValue();					}
	}
}
