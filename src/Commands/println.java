package Commands;

import VirtualMachine.VirtualMachine;

public class println extends Command
{

	@Override
	public void execute (VirtualMachine vm, String[] parameters)
	{
		StringBuilder stringBuilder									= new StringBuilder();

		for (int i = 1; i < parameters.length; i++)					{ stringBuilder.append(vm.getVariable(parameters[i]) + ", ");					}

		if (stringBuilder.length() > 0)								{ System.out.println(stringBuilder.substring(0, stringBuilder.length() - 2));	}
		else														{ System.out.println();															}

		vm.resetReturnValue();
	}
}
