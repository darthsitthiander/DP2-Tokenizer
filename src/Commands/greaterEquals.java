package Commands;

import Tokenizer.TypeHelper;
import VirtualMachine.VirtualMachine;
import enums.BooleanType;

public class greaterEquals extends Command
{

	@Override
	public void execute (VirtualMachine vm, String[] parameters)
	{
		String var1														= vm.getVariable(parameters[1]);
		String var2														= vm.getVariable(parameters[2]);

		if (TypeHelper.isNumeric(var1) && TypeHelper.isNumeric(var2))
		{
			if (Integer.parseInt(var1) >= Integer.parseInt(var2))		{ vm.setReturnValue(BooleanType.TRUE.toString());	}
			else														{ vm.setReturnValue(BooleanType.FALSE.toString());	}
		}
		else
		{
			if (var1.length() >= var2.length())							{ vm.setReturnValue(BooleanType.TRUE.toString());	}
			else														{ vm.setReturnValue(BooleanType.FALSE.toString());	}
		}
	}
}