package Commands;

import Tokenizer.TypeHelper;
import VirtualMachine.VirtualMachine;

public class multiplyMultiplyGet extends Command
{

	@Override
	public void execute (VirtualMachine vm, String[] parameters)
	{
		String var1														= vm.getVariable(parameters[1]);

		if (TypeHelper.isNumeric(var1))
		{
			vm.setVariable(parameters[1], Integer.toString(Integer.parseInt(var1) * Integer.parseInt(var1)));
			vm.setReturnValue(vm.getVariable(parameters[1]));
		}
		else															{ vm.setReturnValue(var1); }
	}
}
