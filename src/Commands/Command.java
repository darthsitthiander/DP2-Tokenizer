package Commands;

import VirtualMachine.VirtualMachine;

public abstract class Command implements Cloneable
{
	public abstract void execute (VirtualMachine vm, String[] parameters);

	public Command clone ()
	{
		Command clone														= null;

		try																	{ clone = (Command) super.clone(); }
		catch (CloneNotSupportedException e)								{ e.printStackTrace(); }

		return clone;
	}
}
