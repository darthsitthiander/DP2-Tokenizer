import Nodes.NodeLinkedList;
import Tokenizer.Tokenizer;
import Nodes.Node;
import Tokenizer.Token;
import Compiler.Compiler;
import VirtualMachine.VirtualMachine;

public class Main
{
	public static void main (String[] args)
	{
		System.out.println("Tokenizer:");

		Tokenizer tokenizer			= new Tokenizer("src/code4a.txt");
		Token startToken			= tokenizer.getFirstToken();

		System.out.println("Compiler:");

		Compiler compiler			= new Compiler();
		NodeLinkedList nodeList		= new NodeLinkedList();
		compiler.compile(startToken, null, nodeList, null);

		Node node					= nodeList.getFirst();

		while (node != null)
		{
			System.out.println(node.toString());
			node = node.getNext();
		}

		System.out.println("VirtualMachine:");

		VirtualMachine.getInstance().run(nodeList.getFirst());
	}
}