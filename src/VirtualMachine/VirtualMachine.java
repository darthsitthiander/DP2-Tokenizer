package VirtualMachine;

import Commands.Command;
import Commands.CommandFactory;
import Nodes.AbstractFunctionCallNode;
import Nodes.Node;
import Visitors.AbstractFunctionCallNodeVisitor;
import Visitors.NextNodeVisitor;

import java.util.HashMap;

public class VirtualMachine
{
	private static VirtualMachine instance;
	private String returnValue;
	private HashMap<String, String> variables;

	private VirtualMachine ()											{ this.variables = new HashMap<>();	}

	public static synchronized VirtualMachine getInstance()
	{
		if (instance == null)											{ instance = new VirtualMachine();	}

		return instance;
	}

	public void setReturnValue (String returnValue)						{ this.returnValue = returnValue;	}
	public String getReturnValue ()										{ return this.returnValue;			}
	public void resetReturnValue ()										{ this.setReturnValue(null);		}

	public void setVariable(String key, String value)					{ this.variables.put(key, value);	}
	public String getVariable(String key)								{ return this.variables.get(key);	}

	public void run (Node node)
	{
		NextNodeVisitor nextNodeVisitor									= new NextNodeVisitor();
		AbstractFunctionCallNodeVisitor abstractFunctionCallNodeVisitor	= new AbstractFunctionCallNodeVisitor();
		AbstractFunctionCallNode abstractFunctionCallNode;
		Command command;

		while (node != null)
		{
			node.accept(abstractFunctionCallNodeVisitor);
			abstractFunctionCallNode									= abstractFunctionCallNodeVisitor.getAbstractFunctionCallNode();

			if (abstractFunctionCallNode != null)
			{
				command													= CommandFactory.getInstance().getCommand(abstractFunctionCallNode.getContentArray()[0]);

				if (command != null)									{ command.execute(this, abstractFunctionCallNode.getContentArray()); }
			}

			node.accept(nextNodeVisitor);
			node														= nextNodeVisitor.getNextNode();
		}
	}
}