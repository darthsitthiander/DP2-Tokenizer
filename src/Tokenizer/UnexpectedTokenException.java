package Tokenizer;

class UnexpectedTokenException extends RuntimeException
{
	public UnexpectedTokenException(Token token)	{ super(token.toString()); }
}