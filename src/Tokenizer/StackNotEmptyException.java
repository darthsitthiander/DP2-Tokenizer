package Tokenizer;

import java.util.Stack;

class StackNotEmptyException extends RuntimeException
{
	private Stack<Token> tokenStack;

	public StackNotEmptyException (Stack<Token> tokenStack)
	{
		super(tokenStack.toString());
		this.tokenStack = tokenStack;
	}

	@Override
	public String getMessage()		{ return "Stack should be empty, but still contains " + tokenStack.size() + " item(s). Stack string: " + super.getMessage(); }
}