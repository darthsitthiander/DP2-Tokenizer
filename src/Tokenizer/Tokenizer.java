package Tokenizer;

import enums.CharacterType;
import enums.TokenType;

import java.io.File;
import java.io.IOException;
import java.util.*;

public class Tokenizer
{
	private String[] content;
	private ArrayList<Token> lToken;
	private Token lastOneLineToken;
	private Stack<Token> tokenStack;
	private boolean foundOneLineLevel, goingToAddOneLineLevel;
	private int level;

	public Tokenizer (String path)
	{
		lToken															= new ArrayList<>();
		tokenStack														= new Stack<>();
		foundOneLineLevel												= false;
		goingToAddOneLineLevel											= false;

		try																{ this.content = readFile(path).split(Character.toString(CharacterType.NEW_LINE.getValue())); }
		catch (IOException e)											{ e.printStackTrace(); }

		this.parse();
	}
	
	private void parse ()
	{
		this.level														= 1;
		String sLine, stringCharacter;
		String text = "";
		char character;
		boolean buildingAString											= false;

		for (int line = 1; line <= this.content.length; line++)
		{
			sLine														= this.content[line-1];

			if (sLine.length() >= 2
					&& sLine.charAt(0) == CharacterType.SINGLE_LINE_COMMENT.getValue()
					&& sLine.charAt(1) == CharacterType.SINGLE_LINE_COMMENT.getValue())
			{
				continue;
			}

			for (int position = 0; position < sLine.length(); position++)
			{
				character												= sLine.charAt(position);
				stringCharacter											= Character.toString(character);

				if (buildingAString)
				{
					text												+= character;
					if (text.charAt(0) == character)					{ buildingAString = false; }
				}
				else
				{
					if (character == CharacterType.STRING_SINGLE_QUOTE.getValue()
						|| character == CharacterType.STRING_DOUBLE_QUOTE.getValue())
					{
						text										+= character;
						buildingAString								= true;
					}
					else if (character == CharacterType.RETURN.getValue()
							|| character == CharacterType.SPACE.getValue())
					{
						text										= text.trim();

						if (text.length() > 0)
						{
							addTokenToList(text, position + 1, line);
							text = "";
						}
					}
					else if (stringCharacter.equals(TokenType.SEMICOLON.getValue())
							|| stringCharacter.equals(TokenType.COMMA.getValue()))
					{
						if (!text.equals(""))						{ addTokenToList(text, position + 1, line); }

						addTokenToList(String.valueOf(character), position + 2, line);

						text										= "";
					}
					else if (stringCharacter.equals(TokenType.ELLIPSIS_OPEN.getValue()))
					{
						text										= text.trim();

						if (text.length() > 0)						{ addTokenToList(text, position + 1, line); }

						addTokenToList(String.valueOf(character), position + 2, line);

						text										= "";

						this.increaseStack(this.lToken.get(this.lToken.size() - 1));
					}
					else if (stringCharacter.equals(TokenType.ELLIPSIS_CLOSE.getValue()))
					{
						text										= text.trim();

						if (text.length() > 0)						{ addTokenToList(text, position + 1, line); }

						this.level--;
						addTokenToList(String.valueOf(character), position + 1, line);
						checkForPartner(this.lToken.get(this.lToken.size() - 1));

						text										= "";

						if (this.lastOneLineToken != null && this.lastOneLineToken.getNext() != null
								&& this.lastOneLineToken.getNext().getPartnerToken() != null
								&& this.lToken.size() == this.lastOneLineToken.getNext().getPartnerToken().getTokenIndex())
						{
							this.foundOneLineLevel					= false;
							this.goingToAddOneLineLevel				= true;
							this.lastOneLineToken 					= null;
						}
					}
					else if (stringCharacter.equals(TokenType.BRACKETS_OPEN.getValue()))
					{
						this.goingToAddOneLineLevel					= false;

						addTokenToList(String.valueOf(character), position + 2, line);

						text										= "";

						this.increaseStack(this.lToken.get(lToken.size() - 1));
					}
					else if (stringCharacter.equals(TokenType.BRACKETS_CLOSE.getValue()))
					{
						this.level--;
						addTokenToList(String.valueOf(character), position + 2, line);
						checkForPartner(this.lToken.get(this.lToken.size() - 1));

						text										= "";
					}
					else											{ text += character; }
				}
			}
		}

		boolean foundUnexpectedToken									= false;
		Token token;

		while (!foundUnexpectedToken && this.tokenStack.size() > 0)
		{
			token														= this.tokenStack.pop();

			if (token.getType() != TokenType.IF)
			{
				foundUnexpectedToken									= true;
				this.tokenStack.push(token);
			}
		}

		if (this.tokenStack.size() > 0 || foundUnexpectedToken)			{ throw new StackNotEmptyException(tokenStack); }

		for (int i = 1; i < this.lToken.size(); i++)
		{
			if (this.lToken.get(i - 1).getType() == this.lToken.get(i).getType()
					&& this.lToken.get(i).getType() != TokenType.ELLIPSIS_OPEN
					&& this.lToken.get(i).getType() != TokenType.ELLIPSIS_CLOSE)
			{
				throw new UnexpectedTokenException(this.lToken.get(i));
			}
		}

		this.lToken.forEach(System.out::println);
	}

	private void checkForPartner (Token token)
	{
		Token previousToken												= this.tokenStack.pop();

		if (token.getLevel() == previousToken.getLevel())
		{
			token.setPartnerToken(previousToken);
			previousToken.setPartnerToken(token);
		}
		else if (previousToken.getType() != TokenType.IF)				{ throw new UnexpectedTokenException(token);  }
	}

	private void addTokenToList (String text, int position, int line)
	{
		Token token														= new Token();

		token.setLevel(this.level);
		token.setValue(text.trim());
		token.setLine_position(position - text.trim().length());
		token.setLine_number(line);
		token.setTokenIndex(this.lToken.size() + 1);

		this.lToken.add(token);

		checkTokenType(token);

		if (this.goingToAddOneLineLevel)
		{
			token.setLevel(token.getLevel() + 1);

			if (token.getType() == TokenType.SEMICOLON)					{ this.goingToAddOneLineLevel = false; }
		}

		if (this.lastOneLineToken != null && this.lastOneLineToken.getNext() == null
				&& this.lastOneLineToken.getType() != TokenType.ELSE)
		{
			this.lastOneLineToken.setNext(token);
		}

		if (this.foundOneLineLevel)
		{
			this.foundOneLineLevel										= false;
			this.lastOneLineToken										= token;

			if (this.lastOneLineToken.getType() == TokenType.ELSE)
			{
				this.foundOneLineLevel									= false;
				this.goingToAddOneLineLevel								= true;
				this.lastOneLineToken									= null;
			}
		}
	}

	private void checkTokenType (Token token)
	{
		Token newToken													= new Token();
		TokenType[] tokenTypes											= TokenType.values();
		String tokenValue												= token.getValue().toLowerCase();

		for (TokenType tokenType : tokenTypes)
		{
			if (tokenValue.equals(tokenType.getValue()))
			{
				token.setType(tokenType);
				break;
			}
		}

		if (tokenValue.length() == 2 && Character.toString(tokenValue.charAt(1)).equals(TokenType.EQUALS.getValue()))
		{
			addOperatorEquals(token);
		}

		if (tokenValue.equals(TokenType.WHILE.getValue())
				|| tokenValue.equals(TokenType.IF.getValue())
				|| tokenValue.equals(TokenType.ELSE.getValue()))
		{
			this.foundOneLineLevel										= true;

			if (tokenValue.equals(TokenType.IF.getValue()))				{ this.tokenStack.add(lToken.get(lToken.size() - 1));	}
			else if (tokenValue.equals(TokenType.ELSE.getValue()))		{ checkForPartner(lToken.get(lToken.size() - 1));		}
		}

		if (token.getType() == null)
		{
			if (TypeHelper.isNumeric(token.getValue()))					{ token.setType(TokenType.NUMBER);						}
			else
			{
				String value											= token.getValue();

				if (value.charAt(0) == CharacterType.STRING_DOUBLE_QUOTE.getValue()
						|| value.charAt(0) == CharacterType.STRING_SINGLE_QUOTE.getValue())
				{
					token.setType(TokenType.STRING);
					token.setValue(token.getValue().substring(1, token.getValue().length() - 1));
				}
				else
				{
					checkShortCutOperationOfIdentifier(token, newToken);

					token.setType(TokenType.IDENTIFIER);
					checkIfIdentifierTokenIsValid(token);
				}
			}
		}
	}

	private void checkShortCutOperationOfIdentifier (Token token, Token newToken)
	{
		String value													= token.getValue();

		if (value.length() > 2)
		{
			boolean foundMatchStart										= false;
			boolean foundMatchEnd										= false;

			String[] operatorArray										= {TokenType.PLUS_PLUS.getValue(), TokenType.MINUS_MINUS.getValue(), TokenType.MULTIPLY_MULTIPLY.getValue(), TokenType.DIVIDE_DIVIDE.getValue()};
			TokenType[] tokenTypeArray									= {TokenType.PLUS_PLUS, TokenType.MINUS_MINUS, TokenType.MULTIPLY_MULTIPLY, TokenType.DIVIDE_DIVIDE};

			newToken.setLevel(token.getLevel());
			newToken.setLine_number(token.getLine_number());

			for (int i = 0; i < operatorArray.length; i++)
			{
				if (value.substring(0, 2).equals(operatorArray[i]) || value.substring(value.length() - 2).equals(operatorArray[i]))
				{
					newToken.setType(tokenTypeArray[i]);
					newToken.setValue(operatorArray[i]);

					if (value.substring(0, 2).equals(operatorArray[i]))	{ foundMatchEnd = true; }
					else												{ foundMatchStart = true; }

					break;
				}
			}

			if (foundMatchStart)
			{
				token.setValue(token.getValue().substring(0, token.getValue().length() - 2));
				newToken.setLine_position(token.getLine_position() + token.getValue().length());
				newToken.setTokenIndex(this.lToken.size() +1);
				this.lToken.add(newToken);
			}
			else if (foundMatchEnd)
			{
				token.setValue(token.getValue().substring(2, token.getValue().length()));
				newToken.setLine_position(token.getLine_position());
				token.setLine_position(token.getLine_position() + 2);
				newToken.setTokenIndex(this.lToken.size() +1);
				this.lToken.set(this.lToken.size() - 1, newToken);
				this.lToken.add(token);
			}
		}
	}

	private static String readFile (String pathname) throws IOException
	{
		File file														= new File(pathname);
		StringBuilder fileContents										= new StringBuilder((int)file.length());
		String lineSeparator											= System.getProperty("line.separator");

		try (Scanner scanner = new Scanner(file))
		{
			while (scanner.hasNextLine())								{ fileContents.append(scanner.nextLine()).append(lineSeparator); }

			return fileContents.toString();
		}
	}

	public Token getFirstToken ()
	{
		LinkedList<Token> tokens										= new LinkedList<>();

		tokens.addFirst(this.lToken.get(0));

		for (int i = 1; i < this.lToken.size(); i++)
		{
			tokens.peekLast().setNext(this.lToken.get(i));
			tokens.addLast(this.lToken.get(i));
		}

		return tokens.peekFirst();
	}

	private void increaseStack (Token stackToken)
	{
		this.tokenStack.add(stackToken);
		this.level++;
	}

	private void checkIfIdentifierTokenIsValid (Token token) {
		if (token.getValue().startsWith(Character.toString(CharacterType.VM_VARIABLE_PREFIX.getValue())))
		{
			throw new IllegalTokenNameException(token);
		}
	}

	private void addOperatorEquals (Token token)
	{
		TokenType[] operatorTypes										= {TokenType.MINUS, TokenType.PLUS, TokenType.MULTIPLY, TokenType.DIVIDE};
		boolean validOperator											= false;
		String operatorString											= Character.toString(token.getValue().charAt(0));

		for (TokenType operatorType : operatorTypes)
		{
			if (operatorString.equals(operatorType.getValue()))
			{
				validOperator = true;
				break;
			}
		}

		if (!validOperator)												{ return; }

		Token previousToken												= this.lToken.get(this.lToken.size() - 2);
		Token newToken													= new Token();

		newToken.setLevel(token.getLevel());
		newToken.setLine_number(token.getLine_number());

		newToken.setType(previousToken.getType());
		newToken.setValue(previousToken.getValue());
		newToken.setLine_position(token.getLine_position() + 2);
		newToken.setTokenIndex(this.lToken.size() + 1);

		if (this.goingToAddOneLineLevel)								{ newToken.setLevel(this.level + 1);	}
		else															{ newToken.setLevel(this.level);		}

		this.lToken.add(newToken);

		newToken														= new Token();

		newToken.setLine_number(token.getLine_number());
		newToken.setLine_position(token.getLine_position() + 2);
		newToken.setTokenIndex(lToken.size() + 1);

		if (this.goingToAddOneLineLevel)								{ newToken.setLevel(this.level + 1);	}
		else															{ newToken.setLevel(this.level);		}

		for (TokenType operatorType : operatorTypes)
		{
			if (operatorString.equals(operatorType.getValue()))
			{
				newToken.setType(operatorType);
				newToken.setValue(operatorType.getValue());
				break;
			}
		}

		this.lToken.add(newToken);
		token.setType(TokenType.EQUALS);
		token.setValue(TokenType.EQUALS.getValue());
	}
}