package Tokenizer;

public class TypeHelper
{
	public static boolean isNumeric (String str)	{ return str.matches("-?\\d+(.\\d+)?"); }

	public static boolean isDouble (String str)		{ return str.matches("-?\\d+(.\\d+)+"); }
}
