package Tokenizer;

import enums.TokenType;

public class Token
{
	private int line_number;
	private int line_position;
	private String value;
	private int level;
	private int tokenIndex;
	private Token partnerToken;
	private TokenType type;
	private Token next;
	
	public int getLine_number ()						{ return line_number; }
	public void setLine_number (int line_number)		{ this.line_number = line_number; }

	public int getLine_position ()			 			{ return line_position; }
	public void setLine_position (int line_position)	{ this.line_position = line_position; }

	public String getValue()							{ return value; }
	public void setValue (String value)					{ this.value = value; }

	public int getLevel ()								{ return level; }
	public void setLevel (int level)					{ this.level = level; }

	public TokenType getType ()							{ return type; }
	public void setType (TokenType type)				{this.type = type; }

	public Token getNext ()								{ return this.next; }
	public void setNext (Token next)					{ this.next = next; }

	public Token getPartnerToken ()						{ return this.partnerToken; }
	public void setPartnerToken (Token partnerToken)	{ this.partnerToken = partnerToken; }

	public int getTokenIndex ()							{ return this.tokenIndex; }
	public void setTokenIndex (int tokenIndex)			{ this.tokenIndex = tokenIndex; }

	public String toString ()							{ return "Tokenizer.Token [line_number: " + line_number + ",\tline_position: " + line_position + ",\ttype: " + type + ",\t\t\t\tvalue: " + value + ",\t\t\tlevel: " + level + ",\t\t\tpartner: " + (partnerToken == null ? 0 : partnerToken.getTokenIndex()) + "]"; }
}
