package Tokenizer;

class IllegalTokenNameException extends RuntimeException
{
	public IllegalTokenNameException(Token token)	{ super(token.toString()); }
}