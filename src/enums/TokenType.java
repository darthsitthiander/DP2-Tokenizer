package enums;

public enum TokenType
{
	ANY								(null),
	IDENTIFIER						(null),
	NUMBER							(null),
	STRING							(null),

	SEMICOLON						(";"),
	COMMA							(","),

	EQUALS							("="),
	EQUALS_EQUALS					("=="),
	NOT_EQUALS						("!="),

	GREATER							(">"),
	LESS							("<"),
	GREATER_EQUALS					(">="),
	LESS_EQUALS						("<="),

	ELLIPSIS_OPEN					("("),
	ELLIPSIS_CLOSE					(")"),
	BRACKETS_OPEN					("{"),
	BRACKETS_CLOSE					("}"),

	MINUS							("-"),
	MINUS_MINUS						("--"),
	PLUS							("+"),
	PLUS_PLUS						("++"),
	MULTIPLY						("*"),
	MULTIPLY_MULTIPLY				("**"),
	DIVIDE							("/"),
	DIVIDE_DIVIDE					("//"),

	AND								("&&"),
	OR								("||"),

	WHILE							("while"),
	IF								("if"),
	ELSE							("else");

	private String value;

	TokenType (String value)		{ this.value = value; }

	public String getValue()		{ return this.value; }
}
