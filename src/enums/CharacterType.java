package enums;

public enum CharacterType
{
	VM_VARIABLE_PREFIX				('$'),
	SINGLE_LINE_COMMENT				('/'),
	STRING_DOUBLE_QUOTE				('"'),
	STRING_SINGLE_QUOTE				('\''),
	RETURN							('\r'),
	NEW_LINE						('\n'),
	SPACE							(' ');

	private char value;

	CharacterType(char value)		{ this.value = value; }

	public char getValue()			{ return this.value; }
}
