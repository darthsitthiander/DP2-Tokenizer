package enums;

public enum FunctionCallType
{
	GET_FROM_RETURN_VALUE			("getFromReturnValue"),
	SET_CONSTANT_TO_RETURN_VALUE	("setConstantToReturnValue"),
	SET_IDENTIFIER_TO_RETURN_VALUE	("setIdentifierToReturnValue"),
	GET_PLUS_PLUS					("getPlusPlus"),
	PLUS_PLUS_GET					("plusPlusGet"),
	GET_MINUS_MINUS					("getMinusMinus"),
	MINUS_MINUS_GET					("minusMinusGet"),
	GET_MULTIPLY_MULTIPLY			("getMultiplyMultiply"),
	MULTIPLY_MULTIPLY_GET			("multiplyMultiplyGet"),
	GET_DIVIDE_DIVIDE				("getDivideDivide"),
	DIVIDE_DIVIDE_GET				("divideDivideGet");

	private String value;

	FunctionCallType (String value)	{ this.value = value; }

	@Override
	public String toString()		{ return this.value; }
}
