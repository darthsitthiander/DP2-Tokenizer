package enums;

public enum BooleanType
{
	TRUE							("true"),
	FALSE							("false");

	private String value;

	BooleanType (String value)		{ this.value = value; }

	@Override
	public String toString()		{ return this.value; }
}
