package Compiler;

import Nodes.Node;
import Nodes.NodeLinkedList;
import Tokenizer.Token;
import enums.TokenType;

import java.util.List;
import java.util.Vector;

public class CompileIf extends CompileIfGeneral
{
	public CompileIf ()													{ super(); }

	public Token compile (Token currentToken, Token endToken, NodeLinkedList nodeList, Node before)
	{
		int level														= currentToken.getLevel();

		nodeList.insertBefore(before, this.firstDoNothingNode);

		List<TokenExpection> expected				= new Vector<>();
		expected.add(new TokenExpection(level, TokenType.IF));
		expected.add(new TokenExpection(level, TokenType.ELLIPSIS_OPEN));
		expected.add(new TokenExpection(level + 1, TokenType.ANY));
		expected.add(new TokenExpection(level, TokenType.ELLIPSIS_CLOSE));
		expected.add(new TokenExpection(level, TokenType.BRACKETS_OPEN));
		expected.add(new TokenExpection(level + 1, TokenType.ANY));
		expected.add(new TokenExpection(level, TokenType.BRACKETS_CLOSE));

		for (TokenExpection expectation : expected)
		{
			if (currentToken == null)									{ return null; }

			if (expectation.Level == level)
			{
				if (currentToken.getType() != expectation.TokenType)
				{
					if (expectation.TokenType == TokenType.BRACKETS_OPEN || (expectation.TokenType == TokenType.BRACKETS_CLOSE && !this.usesBrackets))
					{
						this.usesBrackets								= false;
					}
					else												{ throw new RuntimeException(String.format("Unexpected end of statement, expected %s, but found %s", expectation.TokenType, currentToken.getType())); }
				}
				else													{ currentToken = currentToken.getNext(); }
			}
			else if (expectation.Level >= level)
			{
				if (this.condition == null)
				{
					this.condition										= new CompileCondition();

					currentToken										= this.condition.compile(currentToken, endToken, nodeList, this.conditionalJumpNode);
				}
				else
				{
					this.statementCompiler								= CompilerFactory.getInstance().getCompiler(currentToken);

					currentToken										= this.statementCompiler.compile(currentToken, endToken, nodeList, this.falseDoNothingNode);
				}
			}
		}

		return currentToken;
	}
}
