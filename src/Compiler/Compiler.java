package Compiler;

import Nodes.Node;
import Nodes.NodeLinkedList;
import Tokenizer.Token;
import enums.CharacterType;

public class Compiler implements Cloneable
{
	private static int localCounter = 0;
	protected NodeLinkedList compiled;

	public Compiler ()														{ compiled = new NodeLinkedList(); }

	public Token compile (Token currentToken, Token endToken, NodeLinkedList nodeList, Node before)
	{
		while (currentToken != null)										{ currentToken = CompilerFactory.getInstance().getCompiler(currentToken).compile(currentToken, endToken, nodeList, before); }

		return null;
	}

	public Compiler clone ()
	{
		Compiler clone														= null;

		try
		{
			clone															= (Compiler) super.clone();
			clone.compiled													= new NodeLinkedList();
		}
		catch (CloneNotSupportedException e)								{ e.printStackTrace(); }

		return clone;
	}

	protected String getNextLocalVariableName ()							{ return Character.toString(CharacterType.VM_VARIABLE_PREFIX.getValue()) + (++localCounter);	}
}