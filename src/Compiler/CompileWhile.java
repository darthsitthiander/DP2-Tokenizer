package Compiler;

import Nodes.*;
import Tokenizer.Token;
import enums.TokenType;

import java.util.List;
import java.util.Vector;

public class CompileWhile extends Compiler
{
	private CompileCondition condition;
	private boolean usesBrackets;
	private DoNothingNode firstDoNothingNode;
	private ConditionalJumpNode conditionalJumpNode;
	private JumpNode jumpBackNode;

	public CompileWhile ()												{ super(); }

	public Compiler clone ()
	{
		CompileWhile clone												= (CompileWhile) super.clone();

		clone.usesBrackets												= true;
		clone.firstDoNothingNode										= new DoNothingNode();
		clone.conditionalJumpNode										= new ConditionalJumpNode();
		clone.jumpBackNode												= new JumpNode();

		clone.compiled.addLast(clone.firstDoNothingNode);
		clone.compiled.addLast(clone.conditionalJumpNode);
		clone.compiled.addLast(new DoNothingNode());
		clone.compiled.addLast(clone.jumpBackNode);
		clone.compiled.addLast(new DoNothingNode());

		clone.jumpBackNode.setJumpToNode(clone.compiled.getFirst());
		clone.conditionalJumpNode.setNextOnTrue(clone.compiled.get(2));
		clone.conditionalJumpNode.setNextOnFalse(clone.compiled.getLast());

		return clone;
	}

	public Token compile (Token currentToken, Token endToken, NodeLinkedList nodeList, Node before)
	{
		int level														= currentToken.getLevel();

		nodeList.insertBefore(before, this.firstDoNothingNode);

		List<TokenExpection> expected					= new Vector<>();
		expected.add(new TokenExpection(level, TokenType.WHILE));
		expected.add(new TokenExpection(level, TokenType.ELLIPSIS_OPEN));
		expected.add(new TokenExpection(level + 1, TokenType.ANY));
		expected.add(new TokenExpection(level, TokenType.ELLIPSIS_CLOSE));
		expected.add(new TokenExpection(level, TokenType.BRACKETS_OPEN));
		expected.add(new TokenExpection(level + 1, TokenType.ANY));
		expected.add(new TokenExpection(level, TokenType.BRACKETS_CLOSE));

		for (TokenExpection expectation : expected)
		{
			if (currentToken == null)									{ return null; }

			if (expectation.Level == level)
			{
				if (currentToken.getType() != expectation.TokenType)
				{
					if (expectation.TokenType == TokenType.BRACKETS_OPEN || (expectation.TokenType == TokenType.BRACKETS_CLOSE && !usesBrackets))
					{
						usesBrackets									= false;
					}
					else												{ throw new RuntimeException(String.format("Unexpected end of statement, expected %s, but found %s", expectation.TokenType, currentToken.getType())); }
				}
				else													{ currentToken = currentToken.getNext(); }
			}
			else if (expectation.Level >= level)
			{
				if (this.condition == null)
				{
					this.condition										= new CompileCondition();

					currentToken										= this.condition.compile(currentToken, endToken, nodeList, this.conditionalJumpNode);
				}
				else
				{
					Compiler statementCompiler							= CompilerFactory.getInstance().getCompiler(currentToken);

					currentToken										= statementCompiler.compile(currentToken, endToken, nodeList, this.jumpBackNode);

				}
			}
		}

		return currentToken;
	}
}
