package Compiler;

import enums.TokenType;

import java.util.TreeMap;

public class CompileCondition extends CompileOperator
{
	public CompileCondition ()
	{
		super(new CompileComparison());

		TreeMap<TokenType, String> tokenMap = getTokenMap();

		tokenMap.put(TokenType.AND, "and");
		tokenMap.put(TokenType.OR, "or");
	}
}
