package Compiler;

import Nodes.*;
import Tokenizer.Token;
import enums.TokenType;
import enums.FunctionCallType;

public class CompileSingleStatement extends Compiler
{
	@Override
	public Token compile (Token currentToken, Token endToken, NodeLinkedList nodeList, Node before)
	{
		FunctionCallNode functionCallNode;
		DirectFunctionCallNode directFunctionCallNode;
		int level											= currentToken.getLevel();

		while (currentToken != null && currentToken != endToken && currentToken.getLevel() >= level)
		{
			switch (currentToken.getType())
			{
				case IDENTIFIER:
					Token nextToken							= currentToken.getNext();

					if (nextToken != null && nextToken.getType() == TokenType.ELLIPSIS_OPEN)
					{
						currentToken						= new CompileFunction().compile(currentToken, nextToken.getPartnerToken(), nodeList, before);
					}
					else if (nextToken != null && (
							nextToken.getType() == TokenType.PLUS_PLUS
							|| nextToken.getType() == TokenType.MINUS_MINUS
							|| nextToken.getType() == TokenType.MULTIPLY_MULTIPLY
							|| nextToken.getType() == TokenType.DIVIDE_DIVIDE
							))
					{
						functionCallNode					= new FunctionCallNode();
						functionCallNode.setArraySize(2);

						switch (nextToken.getType())
						{
							case PLUS_PLUS:
								functionCallNode.setAt(0, FunctionCallType.GET_PLUS_PLUS.toString());
								break;
							case MINUS_MINUS:
								functionCallNode.setAt(0, FunctionCallType.GET_MINUS_MINUS.toString());
								break;
							case MULTIPLY_MULTIPLY:
								functionCallNode.setAt(0, FunctionCallType.GET_MULTIPLY_MULTIPLY.toString());
								break;
							case DIVIDE_DIVIDE:
								functionCallNode.setAt(0, FunctionCallType.GET_DIVIDE_DIVIDE.toString());
								break;
						}

						functionCallNode.setAt(1, currentToken.getValue());
						nodeList.insertBefore(before, functionCallNode);

						currentToken						= nextToken.getNext();
					}
					else
					{
						directFunctionCallNode				= new DirectFunctionCallNode();

						directFunctionCallNode.setArraySize(2);
						directFunctionCallNode.setAt(0, FunctionCallType.SET_IDENTIFIER_TO_RETURN_VALUE.toString());
						directFunctionCallNode.setAt(1, currentToken.getValue());

						nodeList.insertBefore(before, directFunctionCallNode);

						currentToken						= currentToken.getNext();
					}
					break;
				case ELLIPSIS_OPEN:
					currentToken							= new CompileCondition().compile(currentToken, endToken, nodeList, before);
					break;
				case NUMBER:
				case STRING:
					directFunctionCallNode					= new DirectFunctionCallNode();
					directFunctionCallNode.setArraySize(2);
					directFunctionCallNode.setAt(0, FunctionCallType.SET_CONSTANT_TO_RETURN_VALUE.toString());
					directFunctionCallNode.setAt(1, currentToken.getValue());

					nodeList.insertBefore(before, directFunctionCallNode);

					currentToken = currentToken.getNext();
					break;
				case PLUS_PLUS:
				case MINUS_MINUS:
				case DIVIDE_DIVIDE:
				case MULTIPLY_MULTIPLY:
					functionCallNode						= new FunctionCallNode();
					functionCallNode.setArraySize(2);

					switch (currentToken.getType())
					{
						case PLUS_PLUS:
							functionCallNode.setAt(0, FunctionCallType.PLUS_PLUS_GET.toString());
							break;
						case MINUS_MINUS:
							functionCallNode.setAt(0, FunctionCallType.MINUS_MINUS_GET.toString());
							break;
						case MULTIPLY_MULTIPLY:
							functionCallNode.setAt(0, FunctionCallType.MULTIPLY_MULTIPLY_GET.toString());
							break;
						case DIVIDE_DIVIDE:
							functionCallNode.setAt(0, FunctionCallType.DIVIDE_DIVIDE_GET.toString());
							break;
					}

					functionCallNode.setAt(1, currentToken.getNext().getValue());
					nodeList.insertBefore(before, functionCallNode);

					currentToken							= currentToken.getNext().getNext();
					break;
				case SEMICOLON:
					nextToken								= currentToken.getNext();
					if (nextToken != null && nextToken.getLevel() >= level)
					{
						currentToken						= CompilerFactory.getInstance().getCompiler(nextToken).compile(nextToken, endToken, nodeList, before);
					}
					else if (nextToken == null)				{ currentToken = null; }
					else									{ currentToken = nextToken; }

					break;
				default:
					currentToken							= currentToken.getNext();
					break;
			}
		}

		return currentToken;
	}
}
