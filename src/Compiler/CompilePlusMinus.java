package Compiler;

import enums.TokenType;

import java.util.TreeMap;

public class CompilePlusMinus extends CompileOperator
{
	public CompilePlusMinus()
	{
		super(new CompileMultiply());

		TreeMap<TokenType,String> tokenMap = this.getTokenMap();

		tokenMap.put(TokenType.PLUS , "plus" );
		tokenMap.put(TokenType.MINUS, "minus" );
	}
}
