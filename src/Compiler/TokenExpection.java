package Compiler;

import enums.TokenType;

public class TokenExpection
{
	public int Level;
	public TokenType TokenType;

	public TokenExpection (int level, TokenType tokenType)
	{
		this.Level		= level;
		this.TokenType	= tokenType;
	}
}
