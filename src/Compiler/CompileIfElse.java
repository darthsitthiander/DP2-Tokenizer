package Compiler;

import Nodes.*;
import Tokenizer.Token;
import enums.TokenType;

import java.util.List;
import java.util.Vector;

public class CompileIfElse extends CompileIfGeneral
{
	private JumpNode jumpNode;
	private DoNothingNode alwaysDoNothingNode;

	public CompileIfElse ()												{ super(); }

	public Compiler clone ()
	{
		CompileIfElse clone												= (CompileIfElse) super.clone();

		clone.jumpNode													= new JumpNode();
		clone.alwaysDoNothingNode										= new DoNothingNode();

		clone.jumpNode.setJumpToNode(clone.alwaysDoNothingNode);

		clone.compiled.insertBefore(clone.falseDoNothingNode, clone.jumpNode);
		clone.compiled.addLast(clone.alwaysDoNothingNode);

		return clone;
	}

	public Token compile (Token currentToken, Token endToken, NodeLinkedList nodeList, Node before)
	{
		int level														= currentToken.getLevel();

		nodeList.insertBefore(before, this.firstDoNothingNode);

		List<TokenExpection> expected									= new Vector<>();

		expected.add(new TokenExpection(level, TokenType.IF));
		expected.add(new TokenExpection(level, TokenType.ELLIPSIS_OPEN));
		expected.add(new TokenExpection(level + 1, TokenType.ANY));
		expected.add(new TokenExpection(level, TokenType.ELLIPSIS_CLOSE));
		expected.add(new TokenExpection(level, TokenType.BRACKETS_OPEN));
		expected.add(new TokenExpection(level + 1, TokenType.ANY));
		expected.add(new TokenExpection(level, TokenType.BRACKETS_CLOSE));
		expected.add(new TokenExpection(level, TokenType.ELSE));
		expected.add(new TokenExpection(level, TokenType.BRACKETS_OPEN));
		expected.add(new TokenExpection(level + 1, TokenType.ANY));
		expected.add(new TokenExpection(level, TokenType.BRACKETS_CLOSE));


		for (TokenExpection expectation : expected)
		{
			if (currentToken == null)									{ return null; }

			if (expectation.Level == level)
			{
				if (expectation.TokenType == TokenType.ELSE)			{ this.usesBrackets = true; }

				if (currentToken.getType() != expectation.TokenType)
				{
					if (expectation.TokenType == TokenType.BRACKETS_OPEN || (expectation.TokenType == TokenType.BRACKETS_CLOSE && !this.usesBrackets))
					{
						this.usesBrackets								= false;
					}
					else												{ throw new RuntimeException(String.format("Unexpected end of statement, expected %s, but found %s", expectation.TokenType, currentToken.getType())); }
				}
				else													{ currentToken = currentToken.getNext(); }
			}
			else if (expectation.Level >= level)
			{
				if (this.condition == null)
				{
					this.condition										= new CompileCondition();

					currentToken										= this.condition.compile(currentToken, endToken, nodeList, this.conditionalJumpNode);
				}
				else if (this.statementCompiler == null)
				{
					this.statementCompiler								= CompilerFactory.getInstance().getCompiler(currentToken);

					currentToken										= this.statementCompiler.compile(currentToken, endToken, nodeList, this.jumpNode);
				}
				else
				{
					Compiler elseCompiler								= CompilerFactory.getInstance().getCompiler(currentToken);

					currentToken										= elseCompiler.compile(currentToken, endToken, nodeList, this.alwaysDoNothingNode);
				}
			}
		}

		return currentToken;
	}
}
