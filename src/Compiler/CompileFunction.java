package Compiler;

import Nodes.*;
import Tokenizer.Token;
import enums.TokenType;
import enums.FunctionCallType;

import java.util.List;
import java.util.TreeMap;
import java.util.Vector;

public class CompileFunction extends CompileOperator
{
	private FunctionCallNode functionCallNode;
	private int parameterIndex;

	public CompileFunction ()
	{
		super(new CompileCondition());

		TreeMap<TokenType, String> tokenMap = getTokenMap();

		tokenMap.put(TokenType.COMMA, "$,");
	}

	@Override
	public Token compile (Token currentToken, Token endToken, NodeLinkedList nodeList, Node before)
	{
		String name;
		int level																	= currentToken.getLevel();
		boolean foundOneParameter													= false;
		int parameterCount															= 0;
		this.parameterIndex															= 0;
		Token beginToken															= currentToken;
		currentToken																= currentToken.getNext().getNext();
		Node[] beforeArray															= new Node[2];
		CompileNextLevel nextLevel													= new CompileNextLevel();
		List<CompileNextLevel> compileNextLevelList									= new Vector<>();

		DirectFunctionCallNode directFunctionCallNode								= new DirectFunctionCallNode();
		directFunctionCallNode.setArraySize(2);
		directFunctionCallNode.setAt(0, FunctionCallType.GET_FROM_RETURN_VALUE.toString());

		nextLevel.setBegin(currentToken);

		while (currentToken != endToken && currentToken.getLevel() >= level)
		{
			if (!foundOneParameter)
			{
				foundOneParameter = true;
				parameterCount++;
			}

			if (this.tokenMap.get(currentToken.getType()) != null)				{ parameterCount++; }

			currentToken														= currentToken.getNext();
		}

		this.functionCallNode													= new FunctionCallNode();

		this.functionCallNode.setArraySize(parameterCount + 1);
		this.functionCallNode.setAt(0, beginToken.getValue());

		currentToken															= beginToken;

		while (currentToken != endToken && currentToken.getLevel() >= level)
		{
			name																= this.tokenMap.get(currentToken.getType());

			if (name != null)
			{
				fillNodeList(name, nodeList, before, beforeArray);

				fillNextLevelList(beforeArray, currentToken, nextLevel, compileNextLevelList);
			}

			currentToken														= currentToken.getNext();
		}

		if (foundOneParameter)
		{
			fillNodeList(this.tokenMap.get(currentToken.getType()), nodeList, before, beforeArray);

			fillNextLevelList(beforeArray, currentToken, nextLevel, compileNextLevelList);
		}
		else
		{
			insertLastNextLevel(endToken, before, nextLevel, compileNextLevelList);
		}

		this.compileNextLevel(nodeList, compileNextLevelList);

		nodeList.insertBefore(before, this.functionCallNode);

		return currentToken;
	}

	protected void fillNodeList (String name, NodeLinkedList nodeList, Node before, Node[] beforeArray)
	{
		DirectFunctionCallNode directFunctionCallNode;

		String tempLocalVarName													= this.getNextLocalVariableName();

		directFunctionCallNode													= new DirectFunctionCallNode();

		directFunctionCallNode.setArraySize(2);
		directFunctionCallNode.setAt(0, FunctionCallType.GET_FROM_RETURN_VALUE.toString());
		directFunctionCallNode.setAt(1, tempLocalVarName);

		beforeArray[0]															= directFunctionCallNode;
		nodeList.insertBefore(before, directFunctionCallNode);

		this.functionCallNode.setAt(++this.parameterIndex, tempLocalVarName);
	}

	private void insertLastNextLevel (Token endToken, Node before, CompileNextLevel nextLevel, List<CompileNextLevel> compileNextLevelList)
	{
		nextLevel.setBefore(before);

		nextLevel.setEnd(endToken);

		compileNextLevelList.add(new CompileNextLevel(nextLevel));
	}

	private void fillNextLevelList (Node[] beforeArray, Token currentToken, CompileNextLevel nextLevel, List<CompileNextLevel> compileNextLevelList)
	{
		nextLevel.setBefore(beforeArray[0]);

		nextLevel.setEnd(currentToken);
		compileNextLevelList.add(new CompileNextLevel(nextLevel));

		currentToken															= currentToken.getNext();

		nextLevel.setBefore(beforeArray[1]);
		nextLevel.setBegin(currentToken);
	}

	protected void compileNextLevel (NodeLinkedList nodeList, List<CompileNextLevel> compileNextLevelList)
	{
		for (CompileNextLevel compileNextLevel : compileNextLevelList)			{ this.nextCompiler.compile(compileNextLevel.getBegin(), compileNextLevel.getEnd(), nodeList, compileNextLevel.getBefore()); }
	}

	protected TreeMap<TokenType, String> getTokenMap()							{ return tokenMap; }
}
