package Compiler;

import Nodes.Node;
import Tokenizer.Token;

public class CompileNextLevel
{
	private Token begin, end;
	private Node before;

	public CompileNextLevel ()
	{
		this.setBegin(null);
		this.setEnd(null);
		this.setBefore(null);
	}

	public CompileNextLevel (CompileNextLevel nextLevel)
	{
		this.setBegin(nextLevel.getBegin());
		this.setEnd(nextLevel.getEnd());
		this.setBefore(nextLevel.getBefore());
	}

	public Token getBegin ()				{ return this.begin;	}
	public Token getEnd ()					{ return this.end;		}
	public Node getBefore ()				{ return this.before;	}

	public void setBegin (Token begin)		{ this.begin = begin;	}
	public void setEnd (Token end)			{ this.end = end;		}
	public void setBefore (Node before)		{ this.before = before;	}
}
