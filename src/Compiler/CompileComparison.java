package Compiler;

import enums.TokenType;

import java.util.TreeMap;

public class CompileComparison extends CompileOperator
{
	public CompileComparison()
	{
		super (new CompileGreaterLessEquals());

		TreeMap<TokenType, String> tokenMap = this.getTokenMap();

		tokenMap.put(TokenType.LESS, "less");
		tokenMap.put(TokenType.GREATER, "greater");
	}
}
