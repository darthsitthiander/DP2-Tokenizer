package Compiler;

import Nodes.*;
import Tokenizer.Token;
import enums.TokenType;
import enums.FunctionCallType;

import java.util.List;
import java.util.Vector;

public class CompileStatement extends CompileOperator
{
	public CompileStatement ()																{ super(new CompileCondition()); }

	public Token compile (Token currentToken, Token endToken, NodeLinkedList nodeList, Node before)
	{
		int level																			= currentToken.getLevel();
		DirectFunctionCallNode directFunctionCallNode										= new DirectFunctionCallNode();

		directFunctionCallNode.setArraySize(2);
		directFunctionCallNode.setAt(0, FunctionCallType.GET_FROM_RETURN_VALUE.toString());

		nodeList.insertBefore(before, directFunctionCallNode);

		List<TokenExpection> expected														= new Vector<>();
		expected.add(new TokenExpection(level, TokenType.IDENTIFIER));
		expected.add(new TokenExpection(level, TokenType.EQUALS));
		expected.add(new TokenExpection(level, TokenType.ANY));

		endToken									= currentToken;

		while (endToken != null && endToken.getType() != TokenType.SEMICOLON)	{ endToken = endToken.getNext(); }

		for (TokenExpection expectation : expected)
		{
			if (currentToken == null)														{ return null; }

			if (currentToken.getType() != expectation.TokenType && expectation.TokenType != TokenType.ANY)
			{
				throw new RuntimeException(String.format("Unexpected end of statement, expected %s, but found %s", expectation.TokenType, currentToken.getType()));
			}
			else
			{
				if (currentToken.getType() == TokenType.IDENTIFIER && directFunctionCallNode.getAt(1) == null)
				{
					directFunctionCallNode.setAt(1, currentToken.getValue());
					currentToken = currentToken.getNext();
				}
				else if (currentToken.getType() == TokenType.EQUALS)						{ currentToken = currentToken.getNext(); }
				else
				{
					Compiler compiler;

					if (currentToken.getNext() != null && currentToken.getNext().getType() == TokenType.ELLIPSIS_OPEN)	{ compiler = new CompileSingleStatement(); }
					else																	{ compiler = new CompileCondition(); }

					currentToken															= compiler.compile(currentToken, endToken, nodeList, directFunctionCallNode);
				}
			}
		}

		while (currentToken != null && currentToken.getType() != TokenType.SEMICOLON)		{ currentToken = currentToken.getNext(); }

		if (currentToken != null)															{ currentToken = currentToken.getNext(); }

		while (currentToken != null && currentToken.getLevel() >= level)
		{
			currentToken																	= CompilerFactory.getInstance().getCompiler(currentToken).compile(currentToken, null, nodeList, before);
		}

		return currentToken;
	}
}