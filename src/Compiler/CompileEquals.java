package Compiler;

import enums.TokenType;

import java.util.TreeMap;

public class CompileEquals extends CompileOperator
{
	public CompileEquals()
	{
		super(new CompilePlusMinus());

		TreeMap<TokenType,String> tokenMap = this.getTokenMap();

		tokenMap.put(TokenType.EQUALS_EQUALS , "equals" );
		tokenMap.put(TokenType.NOT_EQUALS, "notEquals");
	}
}
