package Compiler;

import Nodes.*;
import Tokenizer.Token;
import enums.TokenType;
import enums.FunctionCallType;

import java.util.List;
import java.util.TreeMap;
import java.util.Vector;

public class CompileOperator extends Compiler
{
	protected Compiler nextCompiler;
	protected TreeMap<TokenType, String> tokenMap;

	public CompileOperator(Compiler nextCompiler)
	{
		super();

		this.nextCompiler															= nextCompiler;
		this.tokenMap																= new TreeMap<>();
		this.compiled.addLast(new DoNothingNode());
	}

	@Override
	public Token compile (Token currentToken, Token endToken, NodeLinkedList nodeList, Node before)
	{
		String name;
		int level																	= currentToken.getLevel();
		Node[] beforeArray															= new Node[2];
		CompileNextLevel nextLevel													= new CompileNextLevel();
		List<CompileNextLevel> compileNextLevelList									= new Vector<>();

		nextLevel.setBegin(currentToken);

		while (currentToken != endToken && currentToken.getLevel() >= level)
		{
			name																	= this.tokenMap.get(currentToken.getType());

			if (name != null)
			{
				fillNodeList(name, nodeList, before, beforeArray);

				fillNextLevelList(beforeArray, currentToken, nextLevel, compileNextLevelList);
			}

			currentToken = currentToken.getNext();
		}

		insertLastNextLevel(endToken, before, nextLevel, compileNextLevelList);

		this.compileNextLevel(nodeList, compileNextLevelList);

		return currentToken;
	}

	protected void fillNodeList (String name, NodeLinkedList nodeList, Node before, Node[] beforeArray)
	{
		String[] arguments															= new String[3];
		FunctionCallNode functionCallNode;
		DirectFunctionCallNode directFunctionCallNode;

		arguments[0]																= name;
		arguments[1]																= this.getNextLocalVariableName();
		arguments[2]																= this.getNextLocalVariableName();

		for (int index = 1; index <= 2; index++)
		{
			directFunctionCallNode													= new DirectFunctionCallNode();

			directFunctionCallNode.setArraySize(2);
			directFunctionCallNode.setAt(0, FunctionCallType.GET_FROM_RETURN_VALUE.toString());
			directFunctionCallNode.setAt(1, arguments[index]);

			beforeArray[index - 1]													= directFunctionCallNode;
			nodeList.insertBefore(before, directFunctionCallNode);
		}

		functionCallNode															= new FunctionCallNode();
		functionCallNode.setArraySize(3);

		for (int index = 0; index < 3; index++)										{ functionCallNode.setAt(index, arguments[index]); }

		nodeList.insertBefore(before, functionCallNode);
	}

	private void insertLastNextLevel (Token endToken, Node before, CompileNextLevel nextLevel, List<CompileNextLevel> compileNextLevelList)
	{
		if (compileNextLevelList.size() == 0)										{ nextLevel.setBefore(before); }

		nextLevel.setEnd(endToken);

		compileNextLevelList.add(new CompileNextLevel(nextLevel));
	}

	private void fillNextLevelList (Node[] beforeArray, Token currentToken, CompileNextLevel nextLevel, List<CompileNextLevel> compileNextLevelList)
	{
		if (compileNextLevelList.size() == 0)										{ nextLevel.setBefore(beforeArray[0]); }

		nextLevel.setEnd(currentToken);
		compileNextLevelList.add(new CompileNextLevel(nextLevel));

		currentToken																= currentToken.getNext();

		nextLevel.setBefore(beforeArray[1]);
		nextLevel.setBegin(currentToken);
	}

	protected void compileNextLevel (NodeLinkedList nodeList, List<CompileNextLevel> compileNextLevelList)
	{
		for (CompileNextLevel compileNextLevel : compileNextLevelList)				{ this.nextCompiler.compile(compileNextLevel.getBegin(), compileNextLevel.getEnd(), nodeList, compileNextLevel.getBefore()); }
	}

	protected TreeMap<TokenType, String> getTokenMap()								{ return tokenMap; }
}
