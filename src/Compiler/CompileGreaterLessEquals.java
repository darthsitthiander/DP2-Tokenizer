package Compiler;

import enums.TokenType;

import java.util.TreeMap;

public class CompileGreaterLessEquals extends CompileOperator
{
	public CompileGreaterLessEquals()
	{
		super (new CompileEquals());

		TreeMap<TokenType, String> tokenMap = this.getTokenMap();

		tokenMap.put(TokenType.LESS_EQUALS, "lessEquals");
		tokenMap.put(TokenType.GREATER_EQUALS, "greaterEquals");
	}
}
