package Compiler;

import Tokenizer.Token;
import enums.TokenType;
import java.util.Iterator;
import java.util.ServiceConfigurationError;
import java.util.ServiceLoader;

public class CompilerFactory
{
	private static CompilerFactory instance;
	private ServiceLoader<Compiler> loader;

	private CompilerFactory()							{ loader = ServiceLoader.load(Compiler.class); }

	public static synchronized CompilerFactory getInstance()
	{
		if (instance == null)							{ instance = new CompilerFactory(); }

		return instance;
	}

	private String getClassNameFromToken (Token token)
	{
		String className;

		switch (token.getType())
		{
			case WHILE :
				className								= "CompileWhile";
				break;
			case IF :
				if (token.getPartnerToken() != null && token.getPartnerToken().getType() == TokenType.ELSE)	{ className = "CompileIfElse";	}
				else																						{ className = "CompileIf";		}
				break;
			case IDENTIFIER:
				className								= "CompileStatement";

				if (token.getNext() != null )
				{
					if (token.getNext().getType() == TokenType.ELLIPSIS_OPEN
						|| token.getNext().getType() == TokenType.PLUS_PLUS
						|| token.getNext().getType() == TokenType.MINUS_MINUS
						|| token.getNext().getType() == TokenType.MULTIPLY_MULTIPLY
						|| token.getNext().getType() == TokenType.DIVIDE_DIVIDE)
					{
						className						= "CompileSingleStatement";
					}
				}
				break;
			default:
				className								= "CompileSingleStatement";
				break;
		}

		return className;
	}

	public Compiler getCompiler (final Token token)
	{
		Compiler compilerStrategy						= null;
		Compiler strategy;

		try
		{
			Iterator<Compiler> compilerStrategies		= loader.iterator();

			while (compilerStrategy == null && compilerStrategies.hasNext())
			{
				strategy								= compilerStrategies.next();

				if (strategy.getClass().getSimpleName().equalsIgnoreCase(getClassNameFromToken(token)))
				{
					compilerStrategy					= strategy.clone();
				}
			}
		}
		catch (ServiceConfigurationError serviceError)
		{
			System.out.println("ServiceConfigurationError: " + serviceError.getMessage() + ". Exiting program.");
			System.exit(1);
		}

		return compilerStrategy;
	}
}
