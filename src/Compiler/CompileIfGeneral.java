package Compiler;

import Nodes.ConditionalJumpNode;
import Nodes.DoNothingNode;

public abstract class CompileIfGeneral extends Compiler
{
	protected CompileCondition condition;
	protected Compiler statementCompiler;
	protected ConditionalJumpNode conditionalJumpNode;
	protected DoNothingNode firstDoNothingNode, trueDoNothingNode, falseDoNothingNode;
	protected boolean usesBrackets;

	public CompileIfGeneral ()						{ super(); }

	public Compiler clone ()
	{
		CompileIfGeneral clone						= (CompileIfGeneral) super.clone();

		clone.usesBrackets							= true;
		clone.firstDoNothingNode					= new DoNothingNode();
		clone.conditionalJumpNode					= new ConditionalJumpNode();
		clone.trueDoNothingNode						= new DoNothingNode();
		clone.falseDoNothingNode					= new DoNothingNode();

		clone.conditionalJumpNode.setNextOnTrue(clone.trueDoNothingNode);
		clone.conditionalJumpNode.setNextOnFalse(clone.falseDoNothingNode);

		clone.compiled.addLast(clone.firstDoNothingNode);
		clone.compiled.addLast(clone.conditionalJumpNode);
		clone.compiled.addLast(clone.trueDoNothingNode);
		clone.compiled.addLast(clone.falseDoNothingNode);

		return clone;
	}
}
