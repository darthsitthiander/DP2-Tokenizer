package Compiler;


import enums.TokenType;

import java.util.TreeMap;

public class CompileMultiply extends CompileOperator
{
	public CompileMultiply()
	{
		super(new CompileSingleStatement());

		TreeMap<TokenType, String> tokenMap = getTokenMap();

		tokenMap.put(TokenType.MULTIPLY , "multiply" );
		tokenMap.put(TokenType.DIVIDE, "divide" );
	}
}
