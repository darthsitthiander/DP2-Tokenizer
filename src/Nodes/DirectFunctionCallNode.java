package Nodes;

import Visitors.NodeVisitor;

public class DirectFunctionCallNode extends AbstractFunctionCallNode
{
	public void accept (NodeVisitor visitor)		{ visitor.visit(this); }

	public String toString ()
	{
		String s									= "Direct:\t\t[";

		String[] content							= this.getContentArray();

		for (String aContent : content)				{ s += "\t" + aContent; }

		return s + "\t]";
	}
}
