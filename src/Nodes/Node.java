package Nodes;

import Visitors.NodeVisitor;

public abstract class Node
{
	private Node next;

	public String toString ()			{ return "node";					}

	public Node getNext ()				{ return this.next;					}
	public void setNext (Node node)		{ this.next = node;					}
	public boolean hasNext ()			{ return (this.next != null);		}

	public abstract void accept(NodeVisitor visitor);
}
