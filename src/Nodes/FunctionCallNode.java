package Nodes;

import Visitors.NodeVisitor;

public class FunctionCallNode extends AbstractFunctionCallNode
{
	public void accept (NodeVisitor visitor)		{ visitor.visit(this); }

	public String toString ()
	{
		String s									= "Function:\t[";

		String[] content							= this.getContentArray();

		for (String aContent : content)				{ s += "\t" + aContent; }

		return s + "\t]";
	}
}
