package Nodes;

import Visitors.NodeVisitor;

public class ConditionalJumpNode extends Node
{
	private Node nextOnTrue;
	private Node nextOnFalse;

	public Node getNextOnTrue ()					{ return this.nextOnTrue;			}
	public void setNextOnTrue (Node nextOnTrue)		{ this.nextOnTrue = nextOnTrue;		}

	public Node getNextOnFalse ()					{ return this.nextOnFalse;			}
	public void setNextOnFalse (Node nextOnFalse)	{ this.nextOnFalse = nextOnFalse;	}

	public void accept(NodeVisitor visitor)			{ visitor.visit(this);				}

	public String toString ()						{ return "ConditionalJump";			}
}
