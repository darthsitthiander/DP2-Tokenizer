package Nodes;

import Visitors.NodeVisitor;

public class DoNothingNode extends Node
{
	public void accept(NodeVisitor visitor)		{ visitor.visit(this);			}

	public String toString ()					{ return "DoNothing";			}
}
