package Nodes;

import Visitors.NodeVisitor;

public class JumpNode extends Node
{
	private Node jumpToNode;

	public Node getJumpToNode ()				{ return this.jumpToNode;		}
	public void setJumpToNode (Node jumpToNode)	{ this.jumpToNode = jumpToNode;	}

	public void accept(NodeVisitor visitor)		{ visitor.visit(this);			}

	public String toString ()					{ return "Jump";				}
}
