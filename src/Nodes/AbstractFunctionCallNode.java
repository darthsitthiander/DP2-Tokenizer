package Nodes;

import Visitors.NodeVisitor;

public abstract class AbstractFunctionCallNode extends Node
{
	private String[] contentArray;

	public void setArraySize (int size)
	{
		if (size > 0)								{ contentArray = new String[size];			}
		else										{ contentArray = null;						}
	}

	public void setAt (int index, String text) 		{ contentArray[index] = text;				}
	public String getAt (int index) 				{return  contentArray[index];				}

	public String[] getContentArray ()				{ return contentArray;						}

	public abstract void accept (NodeVisitor visitor);

	public String toString ()						{ return "AbstractFunctionCall";			}
}
